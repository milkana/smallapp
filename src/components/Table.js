import React from "react";
import Header from "./Header";
import Row from "./Row";
import Footer from "./Footer";

function Table(props) {
  return (
    <div className="table-container">
      <Header data={props.headers} />
      <div className="rows">
        {props.rows.map((row, index) => (
          <Row key={`row-${index}`} data={row} />
        ))}
      </div>
      <Footer recordsCount={props.rows.length} />
    </div>
  );
}

export default Table;
