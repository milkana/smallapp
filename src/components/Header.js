import React from "react";

function Header(props) {
  return (
    <div className="header">
      {props.data.map((title, index) => (
        <div key={`header-${index}`} className="cell header-cell">
          {title}
        </div>
      ))}
    </div>
  );
}

export default Header;
