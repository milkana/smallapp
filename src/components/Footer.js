import React from "react";

function Footer(props) {
  return (
    <div className="footer">
      {props.recordsCount} {props.recordsCount === 1 ? "mission" : "missions"}
    </div>
  );
}

export default Footer;
