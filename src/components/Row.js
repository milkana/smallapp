import React from "react";

function Row(props) {
  return (
    <div className="row">
      {Object.keys(props.data).map((key, index) => (
        <div key={`cell-${index}`} className="cell row-cell">
          {props.data[key]}
        </div>
      ))}
    </div>
  );
}

export default Row;
