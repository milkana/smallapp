import Agents from "../data/agents";
import findMaxIsolatedDegreeCountry from "../controllers/isolator";
import React from "react";
import Table from "./Table";

function FEDPage() {
  let isolatedRes = findMaxIsolatedDegreeCountry(Agents);
  let sortedAgents = Agents.sort(
    (a, b) => new Date(a.date).valueOf() - new Date(b.date).valueOf()
  );

  return (
    <div id="fed-page">
      <div className="latge-title">FED Secret Missions</div>
      <div className="container">
        <div className="results-container">
          <span>The most isolated country is {isolatedRes.country}.</span>
          <span>Details: Isolated degree : {isolatedRes.degree}. </span>
          <span>Secret agents : {isolatedRes.isolatedAgents.join(", ")}.</span>
        </div>
      </div>
      <div className="container">
        {sortedAgents.length > 0 ? (
          <Table rows={sortedAgents} headers={Object.keys(sortedAgents[0])} />
        ) : (
          <div>No Agents</div>
        )}
      </div>
    </div>
  );
}

export default FEDPage;
