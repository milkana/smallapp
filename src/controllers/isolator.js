function findIsolatedAgentsByCountry(agentsList) {
  let res = agentsList.reduce(
    ({ countries, agents }, { country, agent }) => {
      //may cause stack overflow in big arrays use concat
      agents[agent] = [...(agents[agent] || []), country];
      countries[country] = [...(countries[country] || []), agent];

      if (agents[agent].length > 1) {
        agents[agent].forEach(
          (country) =>
            (countries[country] = countries[country].filter(
              (item) => item !== agent
            ))
        );
      }
      return { countries, agents };
    },
    { countries: {}, agents: {} }
  );
  return res.countries;
}

function maxIsolatedDegreeCountry(countries) {
  let a = Object.keys(countries).reduce((a, b) =>
    countries[a].length > countries[b].keys ? a : b
  );
  return {
    country: a,
    degree: countries[a].length,
    isolatedAgents: countries[a],
  };
}

function findMaxIsolatedDegreeCountry(agents) {
  return maxIsolatedDegreeCountry(findIsolatedAgentsByCountry(agents));
}

export default findMaxIsolatedDegreeCountry;
