import React from "react";
import FEDPage from "./components/FEDPage";

function App() {
  return (
    <div className="App">
      <FEDPage />
    </div>
  );
}

export default App;
